// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {
	getAuth,
	signInWithPopup,
	GoogleAuthProvider,
	signOut,
	onAuthStateChanged,
	signInWithRedirect,
} from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
	apiKey: "AIzaSyA7sLHohEEI9ljyZ9C5sD2_Tft6-5Lw2I0",
	authDomain: "bi-games.firebaseapp.com",
	projectId: "bi-games",
	storageBucket: "bi-games.appspot.com",
	messagingSenderId: "500994422099",
	appId: "1:500994422099:web:e4869d73418b7e44ce1da6",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const provider = new GoogleAuthProvider();

provider.setCustomParameters({
	prompt: "select_account",
});

export const auth = getAuth();
export const signInWithGooglePopup = async () =>
	await signInWithPopup(auth, provider);
export const signInWithGoogleRedirect = async () =>
	await signInWithRedirect(auth, provider);
export const signOutUser = async () => await signOut(auth);
export const onAuthStateChangedListener = (callback) =>
	onAuthStateChanged(auth, callback);
