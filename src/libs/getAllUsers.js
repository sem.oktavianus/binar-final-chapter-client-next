import config from "../config";

const getAllUsers = async () => {
	const users = await fetch(`${config.baseUrl}/users`);
	if (!users) throw new Error("Failed fetching users data");
	return users.json();
};

export default getAllUsers;
