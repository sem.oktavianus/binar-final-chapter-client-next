import "./globals.scss";
export const metadata = {
	title: "Bi Game",
	description: "Bi Game Aplication",
	icons: {
		icon: "/assets/images/icon.ico",
	},
};
// import { Provider } from 'react-redux';
// import { PersistGate } from 'redux-persist/lib/integration/react';
import { Providers } from "./store/provider";

const RootLayout = ({ children }) => {
	return (
		<html lang="en">
			<body>
				<Providers>{children}</Providers>
				<script src="/assets/js/bootstrap.bundle.js" async />
				<script src="/assets/js/bootstrap.js" async />
			</body>
		</html>
	);
};
export default RootLayout;
