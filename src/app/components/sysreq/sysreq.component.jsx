function Sysreq() {
	return (
		<>
			<section
				className="system-req d-flex flex-column justify-content-center align-items-center"
				id="sys-req"
			>
				<div className="container">
					<p className="sub-text text-center mb-5">
						can my computer run this game?
					</p>
					<div className="row">
						<div className="col-12 col-lg-6">
							<div className="content mt-4">
								<h2 className="h2 mb-3">system requirments</h2>
								<div className="row row-cols-2">
									<div className="col border border-bottom-0 border-end-0 border-dark">
										<h3 className="h3">os:</h3>
										<div className="body-sentence">
											Windows 7 64 bit only (no OSX Support at this time)
										</div>
									</div>
									<div className="col border border-dark border-bottom-0">
										<h3 className="h3">processor:</h3>
										<div className="body-sentence">
											Intel Core 2 Duo @ 2.4 GHZ or AMD Athion X2 @ 2.8 GHZ
										</div>
									</div>
									<div className="col border border-bottom-0 border-end-0 border-dark">
										<h3 className="h3">memory:</h3>
										<div className="body-sentence">4 GB RAM</div>
									</div>
									<div className="col border border-dark border-bottom-0">
										<h3 className="h3">storage:</h3>
										<div className="body-sentence">8 GB availlable space</div>
									</div>

									<div className="col-12 border border-dark">
										<h3 className="h3">graphics:</h3>
										<div className="body-sentence">
											NVIDIA GeForce GTX 660 2GB or AMD Radeon HD 7850 2GB
											DirectX11 (Shader Model 5)
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</>
	);
}

export default Sysreq;
