"use client";
/* eslint-disable jsx-a11y/anchor-is-valid */
//import { useContext, useEffect } from "react";
import Link from "next/link";
import { signOutUser } from "../../../libs/firebase/firebase.libs";
import { useDispatch, useSelector } from "react-redux";
import { setUserLogout } from "../../store/user/user.action";
import { selectCurrentUser } from "../../store/user/user.selector";
//import { UserContext } from "../../context/user.context";
function Header() {
	const currentUser = useSelector(selectCurrentUser);
	const dispatch = useDispatch();
	//const { currentUser, setCurrentUser } = useContext(UserContext);
	const onclickButtonLogOut = (e) => {
		dispatch(setUserLogout());
		signOutUser();
	};
	return (
		<>
			<nav className="navbar navbar-expand-lg bg-dark-trans px-5 py-4">
				<div className="container">
					<Link className="navbar-brand link-light" href="/landing">
						Logo
					</Link>
					<button
						className="navbar-toggler navbar-dark"
						type="button"
						data-bs-toggle="collapse"
						data-bs-target="#navbarNav"
						aria-controls="navbarNav"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						<span className="navbar-toggler-icon-modif">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								width="32"
								height="32"
								fill="currentColor"
								className="bi bi-list"
								viewBox="0 0 16 16"
							>
								<path
									fillRule="evenodd"
									d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"
								/>
							</svg>
						</span>
					</button>
					<div
						className="collapse navbar-collapse justify-content-between"
						id="navbarNav"
					>
						<ul className="navbar-nav">
							<li className="nav-item">
								<Link className="nav-link" href="/landing">
									Home
								</Link>
							</li>
							<li className="nav-item">
								<Link className="nav-link" href="/landing/games">
									Game List
								</Link>
							</li>
							<li className="nav-item">
								<Link className="nav-link" href="/landing/users">
									User List
								</Link>
							</li>
							<li className="nav-item">
								{
									<Link className="nav-link" href={`/landing/users/${currentUser?.id}`}>
										{currentUser ? "My Score " + currentUser.score : ""}
									</Link>
								}
							</li>
						</ul>
						{currentUser ? (
							<ul className="navbar-nav">
								<li className="nav-item">
									<Link className="nav-link" href={`landing/users/${currentUser?.id}`}>
										Hi, {currentUser.name}
									</Link>
								</li>
								<li className="nav-item">
									<Link
										onClick={onclickButtonLogOut}
										className="nav-link"
										href="/landing"
									>
										Log Out
									</Link>
								</li>
							</ul>
						) : (
							<ul className="navbar-nav">
								<li className="nav-item">
									<Link className="nav-link" href="/landing/register">
										Register
									</Link>
								</li>
								<li className="nav-item">
									<Link className="nav-link" href="/landing/login">
										Login
									</Link>
								</li>
							</ul>
						)}
					</div>
				</div>
			</nav>
			
		</>
	);
}

export default Header;
