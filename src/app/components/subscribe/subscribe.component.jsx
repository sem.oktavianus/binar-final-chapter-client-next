function Subscribe() {
	return (
		<>
			<section className="subscribe d-flex justify-content-center align-items-center">
				<div className="container">
					<div className="row">
						<div className="d-none d-lg-block col-lg-6">
							<div className="subscribe-image m-auto"></div>
						</div>
						<div className="col-lg-6 align-self-center">
							<p className="sub-text mb-4">Want to stay in touch?</p>
							<h2 className="h2 mb-4">newsletter SUBSCRIBE</h2>
							<p className="body-sentence mb-5">
								In order to start receiving our news, all you have to do is
								enter your email address. Everything else will be taken care of
								by us. We will send you emails containing information about
								game. We don’t spam.
							</p>
							<form action="submit">
								<div className="d-flex flex-column gap-3 flex-xl-row">
									<div className="input-cover">
										<input
											type="email"
											className="flex-grow-1"
											placeholder="youremail@binar.co.id"
											id="email"
										/>
										<label htmlFor="email">Your email address</label>
									</div>
									<button
										type="button"
										className="btn btn-warning fw-bold button-main"
									>
										Subscribe now
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		</>
	);
}

export default Subscribe;
