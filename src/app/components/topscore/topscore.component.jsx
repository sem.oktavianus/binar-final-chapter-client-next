function Topscore() {
	return (
		<>
			<section
				className="top-scores d-flex flex-column justify-content-center align-items-center"
				id="top-scores"
			>
				<div className="container">
					<div className="row">
						<div className="col-12 col-lg-6 mb-5 d-flex flex-column justify-content-center align-items-start">
							<h2 className="h2 mb-3">top scores</h2>
							<div className="body-sentence mb-5">
								This top scores from various games provided on this platform
							</div>
							<button
								type="button"
								className="btn btn-warning fw-bold button-main"
							>
								see more
							</button>
						</div>
						<div className="top-scores__card col-12 col-lg-6 d-flex flex-column">
							<div className="custom-card mb-3 align-self-end">
								<div className="custom-card__head mb-3">
									<div className="custom-card__head--avatar">
										<img
											src="/assets/images/evan-lahti.jpg"
											className="avatar"
											alt="evan-lahti"
										/>
									</div>
									<div className="custom-card__head--user me-auto">
										<p className="name">evan lahti</p>
										<p className="title">pc gamer</p>
									</div>
									<div className="custom-card__head--icon">
										<img src="/assets/images/twitter-card.png" alt="" />
									</div>
								</div>
								<div className="custom-card__body">
									<p className="body-sentence mb-2">
										“One of my gaming highlights of the year.”
									</p>
									<p className="date">June 18, 2021</p>
								</div>
							</div>
							<div className="custom-card mb-3 align-self-start">
								<div className="custom-card__head mb-3">
									<div className="custom-card__head--avatar">
										<img
											src="/assets/images/jada-griffin.jpg"
											className="avatar"
											alt="evan-lahti"
										/>
									</div>
									<div className="custom-card__head--user me-auto">
										<p className="name">Jada Griffin</p>
										<p className="title">Nerdreactor</p>
									</div>
									<div className="custom-card__head--icon">
										<img src="/assets/images/twitter-card.png" alt="" />
									</div>
								</div>
								<div className="custom-card__body">
									<p className="body-sentence mb-2">
										“The next big thing in the world of streaming and survival
										games.”
									</p>
									<p className="date">July 10, 2021</p>
								</div>
							</div>
							<div className="custom-card align-self-end me-5">
								<div className="custom-card__head mb-3">
									<div className="custom-card__head--avatar">
										<img
											src="/assets/images/aaron-williams.jpg"
											className="avatar"
											alt="evan-lahti"
										/>
									</div>
									<div className="custom-card__head--user me-auto">
										<p className="name">Aaron Williams</p>
										<p className="title">Uproxx</p>
									</div>
									<div className="custom-card__head--icon">
										<img src="/assets/images/twitter-card.png" alt="" />
									</div>
								</div>
								<div className="custom-card__body">
									<p className="body-sentence mb-2">
										“Snoop Dogg Playing The Wildly Entertaining ‘SOS’ Is
										Ridiculous.”
									</p>
									<p className="date">December 24, 2018</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</>
	);
}

export default Topscore;
