function Footer() {
	return (
		<>
			<section className="footer">
				<div className="container">
					<nav className="navbar">
						<ul className="footer-nav ms-auto me-5 d-flex flex-column flex-lg-row gap-4">
							<li>
								<a href="#header">main</a>
							</li>
							<li>
								<a href="#the-game">about</a>
							</li>
							<li>
								<a href="#features">game features</a>
							</li>
							<li>
								<a href="#sys-req">system requirments</a>
							</li>
							<li>
								<a href="#top-scores">quotes</a>
							</li>
						</ul>
						<ul className="footer-nav d-flex flex-column flex-lg-row gap-4">
							<li>
								<a href="#">
									<img src="/assets/images/facebook.svg" alt="facebook icon" />
								</a>
							</li>
							<li>
								<a href="#">
									<img src="/assets/images/twitter.svg" alt="tweeter icon" />
								</a>
							</li>
							<li>
								<a href="#features">
									<img src="/assets/images/vector.svg" alt="youtube-icon" />
								</a>
							</li>
							<li>
								<a href="#">
									<img src="/assets/images/twitch.svg" alt="twitch icon" />
								</a>
							</li>
						</ul>
					</nav>
					<hr />
					<div className="d-flex align-items-baseline justify-content-between">
						<p className="copyright">
							&copy; 2018 Your Games, Inc, All Rights Reserved
						</p>
						<nav className="navbar">
							<ul>
								<li>
									<a href="#">privasi policy</a>
								</li>
								<div className="vr"></div>
								<li>
									<a href="#">terms of services</a>
								</li>
								<div className="vr"></div>
								<li>
									<a href="#">code of conduct</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</section>
		</>
	);
}

export default Footer;
