import { applyMiddleware, legacy_createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import { rootReducer } from "./root-reducer";

const middlewares = [thunk];

const persistConfig = {
	key: "root",
	storage,
	whitelist: ["user"], // replace with your own reducer names
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

// Use composeWithDevTools to enable Redux DevTools extension
// apply middleware and enhancers
export const store = legacy_createStore(
	persistedReducer,
	composeWithDevTools(applyMiddleware(...middlewares))
);

// Create a persistor object
export const persistor = persistStore(store);
