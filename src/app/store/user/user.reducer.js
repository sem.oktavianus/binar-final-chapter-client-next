import USER_ACTION_TYPES from "./user.types";

const INITIAL_STATE = {
	currentUser: null,
	isLoading: false,
	error: null,
};

export const userReducer = (state = INITIAL_STATE, action) => {
	const { type, payload } = action;

	switch (type) {
		case USER_ACTION_TYPES.SET_FETCH_USER_START:
			return { ...state, isLoading: true };
		case USER_ACTION_TYPES.SET_FETCH_USER_SUCCESS:
			return { ...state, isLoading: false, currentUser: payload };
		case USER_ACTION_TYPES.SET_FETCH_USER_ERROR:
			return { ...state, isLoading: false, error: payload };
		case USER_ACTION_TYPES.SET_CURRENT_USER:
			return { ...state, currentUser: payload };
		case USER_ACTION_TYPES.SET_USER_LOGOUT:
			return { ...INITIAL_STATE };
		default:
			return state;
	}
};
