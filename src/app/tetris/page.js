"use client";
import React from "react";
import { useSelector } from "react-redux";
import { selectCurrentUser } from "../store/user/user.selector";
import { redirect } from "next/navigation";


const Tetris = () => {
	const currentUser = useSelector(selectCurrentUser);
	return currentUser ? (
		<h1 className="text-white display-1">TETRIS GAME</h1>
	) : (
		redirect("/landing/login")
	);
};

export default Tetris;
