import Hero from '../components/hero/hero.component'
import Thegame from '../components/thegame/thegame.component'
import Features from '../components/features/features.component'
import Sysreq from '../components/sysreq/sysreq.component'
import Topscore from '../components/topscore/topscore.component'
import Subscribe from '../components/subscribe/subscribe.component'
import Footer from '../components/footer/footer.component'

export default function Landing() {
  return (
   
    <div>
	<Hero />
	<Thegame />
	<Features />
	<Sysreq />
	<Topscore />
	<Subscribe />
	<Footer />
    </div>
							
							
  )
}
