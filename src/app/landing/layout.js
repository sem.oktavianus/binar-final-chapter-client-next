import Header from "../components/header/header.component";

const LandingLayout = ({ children }) => {
	return (
		<>
			<Header />
			{children}
		</>
	);
};
export default LandingLayout;
