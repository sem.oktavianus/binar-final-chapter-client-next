import Link from "next/link";
import getAllUsers from "../../../libs/getAllUsers";

async function UserList() {
	const users = await getAllUsers();
	return (
		<div className="users">
			<div className="h2 mb-4">User List</div>
			<div className="box">
				<table className="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Name</th>
							<th scope="col">Email</th>
							<th scope="col">City</th>
							<th scope="col">Score</th>
							<th scope="col">Detail</th>
						</tr>
					</thead>
					<tbody>
						{users.map((user, index) => {
							return (
								<>
									<tr key={index}>
										<th scope="row">{index + 1}</th>
										<td>{user.data.name}</td>
										<td>{user.data.email}</td>
										<td>{user.data.city}</td>
										<td>{user.data.score}</td>
										<td>
											<Link href={`/landing/users/${user?.id}`}>
												<button
													type="button"
													className="btn btn-warning btn-sm"
												>
													Detail
												</button>
											</Link>
										</td>
									</tr>
								</>
							);
						})}
					</tbody>
				</table>
			</div>
		</div>
	);
}

export default UserList;
