"use client";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { selectCurrentUser } from "../../../store/user/user.selector";
import Link from "next/link";
import axios from "axios";
import config from "../../../../config";

function UserDetail({ params }) {
	const currentUser = useSelector(selectCurrentUser);
	const [user, setUser] = useState({});
	const { id } = params;
	useEffect(() => {
		axios.get(`${config.baseUrl}/users/${id}`, {}).then((res) => {
			setUser(res.data.data);
		});
	}, [id, currentUser]);

	return (
		<>
			<div className="users">
				<div className="h2 mb-4">User Detail</div>
				<div className="box">
					<div className="d-flex flex-sm-row flex-column">
						<div className="avatar">
							{user.avatar_url ? (
								<img className="avatar" src={user.avatar_url} />
							) : (
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="200"
									height="200"
									fill="currentColor"
									class="bi bi-person-circle"
									viewBox="0 0 16 16"
								>
									<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
									<path
										fill-rule="evenodd"
										d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"
									/>
								</svg>
							)}
						</div>

						<table className="table detail">
							<tbody>
								<tr>
									<td>Nama</td>
									<td>{user.name}</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>{user.email}</td>
								</tr>
								<tr>
									<td>City</td>
									<td>{user.city || ""}</td>
								</tr>
								<tr>
									<td>Biodata</td>
									<td>{user.biodata || ""}</td>
								</tr>
								<tr>
									<td>Total Score</td>
									<td>{user.score || 0}</td>
								</tr>
								<tr>
									<td>social media</td>
									<td>{user.socialMedia || ""}</td>
								</tr>
							</tbody>
						</table>
					</div>
					{currentUser?.email === user.email ? (
						<Link href={`/landing/users/${currentUser?.id}/edit`}>
							<button
								type="submit"
								className="btn btn-warning fw-bold button-main mt-5"
							>
								Edit My Profile
							</button>
						</Link>
					) : null}
				</div>
			</div>
		</>
	);
}

export default UserDetail;
