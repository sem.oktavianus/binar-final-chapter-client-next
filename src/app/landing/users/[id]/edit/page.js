"use client";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setUserUpdate } from "../../../../store/user/user.action";
import {
	selectCurrentUser,
	selectIsLoading,
} from "../../../../store/user/user.selector";
import { useRouter } from "next/navigation";

function EditUser({ params }) {
	const router = useRouter();
	const currentUser = useSelector(selectCurrentUser);
	const isLoading = useSelector(selectIsLoading);
	const dispatch = useDispatch();
	const { id } = params;
	//const { currentUser, setCurrentUser } = useContext(UserContext);
	const [user, setUser] = useState(currentUser);
	const updatehandler = async (e) => {
		e.preventDefault();
		// const resUpdate = await axios.put(`${config.baseUrl}/users/update/${id}`, {
		// 	...user,
		// });
		dispatch(setUserUpdate({ ...user }));
		setTimeout(() => {
			router.push(`/landing/users/${id}`);
		}, 1000);
	};
	return (
		<>
			<div className="users">
				<div className="h2 mb-4">User Detail</div>
				<div className="box">
					<form action="" method="post" encType="multipart/form-data">
						<table className="table detail table-borderless">
							<tbody>
								<tr>
									<td>Nama</td>
									<td>
										<div className="input-cover">
											<input
												type="text"
												placeholder={user.name}
												id="fullname"
												name="fullname"
											/>
											<label htmlFor="fullname">Full Name</label>
										</div>
									</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>
										<div className="input-cover">
											<input
												type="email"
												id="email"
												name="email"
												placeholder={user.email}
												onChange={(e) => {
													setUser({ ...user, email: e.target.value });
												}}
											/>
											<label htmlFor="email">Email</label>
										</div>
									</td>
								</tr>
								<tr>
									<td>City</td>
									<td>
										<div className="input-cover">
											<input
												type="text"
												placeholder={user.city}
												id="city"
												name="city"
												onChange={(e) => {
													setUser({ ...user, city: e.target.value });
												}}
											/>
											<label htmlFor="city">City</label>
										</div>
									</td>
								</tr>
								<tr>
									<td>Biodata</td>
									<td>
										<div className="input-cover">
											<input
												type="text"
												placeholder={user.biodata}
												id="biodata"
												name="biodata"
												onChange={(e) => {
													setUser({ ...user, biodata: e.target.value });
												}}
											/>
											<label htmlFor="biodata">Biodata</label>
										</div>
									</td>
								</tr>
								<tr>
									<td>social media</td>
									<td>
										<div className="input-cover">
											<input
												type="text"
												placeholder={user.socialMedia}
												id="sosmed"
												name="sosmed"
												onChange={(e) => {
													setUser({ ...user, socialMedia: e.target.value });
												}}
											/>
											<label htmlFor="sosmed">Full Name</label>
										</div>
									</td>
								</tr>
								<tr>
									<td>Avatar</td>
									<td>
										<input
											type="file"
											id="avatar"
											name="avatar"
											className="form-control"
											onChange={(e) => {
												setUser({ ...user, avatar: e.target.files[0] });
											}}
										/>
									</td>
								</tr>
							</tbody>
						</table>
						{currentUser?.id === id ? (
							<button
								type="submit"
								className="btn btn-warning fw-bold button-main mt-5"
								onClick={updatehandler}
							>
								{isLoading ? "Loading..." : "Edit My Profile"}
							</button>
						) : null}
					</form>
				</div>
			</div>
		</>
	);
}

export default EditUser;
