"use client";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { redirect } from "next/navigation";
import {
	setCurrentUser,
	setCurrentUserWithGoogle,
} from "../../store/user/user.action";
import {
	selectCurrentUser,
	selectIsLoading,
	selectError,
} from "../../store/user/user.selector";
import { FcGoogle } from "react-icons/fc";
import Link from "next/link";
import { useRouter } from "next/navigation";

function Login() {
	const router = useRouter();
	const isLoading = useSelector(selectIsLoading);
	const error = useSelector(selectError);
	const dispatch = useDispatch();
	const currentUser = useSelector(selectCurrentUser);
	const [password, setPassword] = useState("");
	const [email, setEmail] = useState("");
	const logGoogleUser = () => {
		dispatch(setCurrentUserWithGoogle());
		currentUser && router.push("/landing");
	};
	const loginHandler = (e) => {
		e.preventDefault();

		dispatch(setCurrentUser(email, password));
		currentUser && router.push("/landing");
	};

	useEffect(() => {
		if (currentUser) {
			router.push("/landing");
		}
	}, [currentUser]);
	return (
		<>
			<div className="container">
				<div className="d-flex justify-content-center mt-2">
					<div className="card custom-card">
						<div className="card-header">
							<h2 className="h2 text-center">Sign In</h2>
						</div>
						<div className="card-body">
							<form method="post">
								<div className="d-flex flex-column gap-3">
									<div className="input-cover">
										<input
											type="email"
											placeholder="youremail@binar.co.id"
											id="username"
											name="email"
											value={email}
											onChange={(e) => {
												setEmail(e.target.value);
											}}
										/>
										<label htmlFor="username">Username</label>
									</div>
									<div className="input-cover">
										<input
											type="password"
											placeholder="********"
											id="password"
											name="password"
											value={password}
											onChange={(e) => {
												setPassword(e.target.value);
											}}
										/>
										<label htmlFor="password">Password</label>
									</div>
									<button
										onClick={loginHandler}
										type="submit"
										className="btn btn-warning fw-bold button-main"
									>
										{isLoading ? "LOADING..." : "LOG IN"}
									</button>
								</div>
							</form>
						</div>
						<div className="card-footer">
							<div className="d-flex justify-content-center links fs-3 mb-3 text-light">
								<p>
									Dont have an account?{" "}
									<Link href="/landing/register"> Register</Link>
								</p>
							</div>

							<p className="fs-3 text-light text-center">
								Or Sign In with{" "}
								<FcGoogle
									onClick={logGoogleUser}
									style={{ fontSize: "40px", cursor: "pointer" }}
								/>
							</p>
						</div>
						{error && (
							<div
								className="alert alert-warning alert-dismissible fade show mt-3"
								role="alert"
							>
								<strong>Login Failed!</strong> Please try again
								<button
									type="button"
									className="btn-close"
									data-bs-dismiss="alert"
									aria-label="Close"
								></button>
							</div>
						)}
					</div>
				</div>
			</div>
		</>
	);
}

export default Login;
