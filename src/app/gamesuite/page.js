"use client";
import { useReducer } from "react";
import { useSelector, useDispatch } from "react-redux";
import { redirect } from "next/navigation";
import { setUserScore } from "../store/user/user.action";
import {
	selectCurrentUser,
	selectUserScore,
	selectIsLoading,
} from "../store/user/user.selector";
import "./gamesuite.style.scss";
import {
	randomChoice,
	INITIAL_STATE,
	GameSuiteReducer,
} from "./gameSuiteLogic";

function GameSuite() {
	const currentUser = useSelector(selectCurrentUser);
	const isLoading = useSelector(selectIsLoading);
	const globDispatch = useDispatch();
	const [state, dispatch] = useReducer(GameSuiteReducer, INITIAL_STATE);
	const userScore = useSelector(selectUserScore) || 0;
	const onUpdateScore = async () => {
		globDispatch(
			setUserScore(currentUser, state.score + (currentUser?.score || 0))
		);
		dispatch({ type: "REFRESH" });
	};
	return (
		<>
			{currentUser ? (
				<div>
					<div className="container-fluid">
						{/**<!-- Title --> */}

						<div className="title">
							<p>
								<a href="/landing">back</a>
							</p>
							<img
								src="/assets/images/batu-gunting-kertas/logo 1.png"
								alt="logo"
							/>
							<h1>ROCK PAPER SCISSORS</h1>
							<p>{state.result}</p>
							<h1 className="score">Score: {state.score}</h1>
						</div>
						{/*<!-- Main  --> */}
						<div></div>
						<div className="main">
							<div className="player">
								<p>Player 1</p>
								<div
									className={
										state.playerChoice == "batu"
											? "img-box-player selected"
											: "img-box-player"
									}
									value="batu"
									onClick={(e) => {
										dispatch({
											type: "PLAYER.BATU",
											payload: { playerOne: "batu", com: randomChoice() },
										});
									}}
								>
									<img
										src="/assets/images/batu-gunting-kertas/batu.png"
										alt="batu"
									/>
								</div>
								<div
									className={
										state.playerChoice == "kertas"
											? "img-box-player selected"
											: "img-box-player"
									}
									value="kertas"
									onClick={(e) => {
										dispatch({
											type: "PLAYER.KERTAS",
											payload: { playerOne: "kertas", com: randomChoice() },
										});
									}}
								>
									<img
										src="/assets/images/batu-gunting-kertas/kertas.png"
										alt="kertas"
									/>
								</div>
								<div
									className={
										state.playerChoice == "gunting"
											? "img-box-player selected"
											: "img-box-player"
									}
									value="gunting"
									onClick={(e) => {
										dispatch({
											type: "PLAYER.GUNTING",
											payload: { playerOne: "gunting", com: randomChoice() },
										});
									}}
								>
									<img
										src="/assets/images/batu-gunting-kertas/gunting.png"
										alt="gunting"
									/>
								</div>
							</div>
							<div className="spacing">
								<p className={state.result ? "result draw-result" : ""}>VS</p>
							</div>
							<div className="computer">
								<p>Com</p>
								<div
									className={
										state.comChoice == "batu"
											? "img-box-com selected"
											: "img-box-com"
									}
								>
									<img
										src="/assets/images/batu-gunting-kertas/batu.png"
										alt="batu"
									/>
								</div>
								<div
									className={
										state.comChoice == "kertas"
											? "img-box-com selected"
											: "img-box-com"
									}
								>
									<img
										src="/assets/images/batu-gunting-kertas/kertas.png"
										alt="kertas"
									/>
								</div>
								<div
									className={
										state.comChoice == "gunting"
											? "img-box-com selected"
											: "img-box-com"
									}
								>
									<img
										src="/assets/images/batu-gunting-kertas/gunting.png"
										alt="gunting"
									/>
								</div>
							</div>
						</div>
						{/*<!--Refresh button-->*/}

						<div
							className="refresh"
							onClick={(e) => {
								dispatch({ type: "REFRESH" });
							}}
						>
							<img
								src="/assets/images/batu-gunting-kertas/refresh.png"
								alt="refresh"
							/>
							<button
								type="button"
								className="btn btn-success btn-lg mb-3"
								onClick={onUpdateScore}
							>
								{isLoading ? "Loading..." : "Save Score"}
							</button>
							<h1 style={{ fontWeight: "600" }}>My Total Score: {userScore}</h1>
						</div>
					</div>
				</div>
			) : (
				redirect("/landing/login")
			)}
		</>
	);
}

export default GameSuite;
