/**
 * a Class to selecting the DOM elemet from the Html layout
 */
class GameLayout {
  constructor() {}
  static playerOneChoices() {
    return document.querySelectorAll(".img-box-player");
  }
  static playerTwoChoices() {
    return document.querySelectorAll(".img-box-com");
  }
  static result() {
    return document.querySelector(".spacing p");
  }
  static refresh() {
    return document.querySelector(".refresh img");
  }
  static playSound(result) {
    return document.querySelector(`audio[data-result="${result}"]`);
  }
}

/**
 * a class to generate conclution win or lose between the players
 * @constructor
 * @param {string} choiceOne
 * @param {string} choiceTwo
 */
class Result {
  constructor(choiceOne, choiceTwo) {
    this.choiceOne = choiceOne;
    this.choiceTwo = choiceTwo;
  }
  /**
   * method to generate result
   * @method
   */
  getResult() {
    switch (this.choiceOne + this.choiceTwo) {
      case "batubatu":
        return "draw";
      case "batukertas":
        return "player 2 win";
      case "batugunting":
        return "player 1 win";
      case "kertasbatu":
        return "player 1 win";
      case "kertaskertas":
        return "draw";
      case "kertasgunting":
        return "player 2 win";
      case "guntingbatu":
        return "player 2 win";
      case "guntingkertas":
        return "player 1 win";
      case "guntinggunting":
        return "draw";
      default:
        return;
    }
  }
  /**
   * method to set layout after matching two choiches beetwen player 1 and player 2
   * @method
   */
  setResultLayout() {
    if (this.getResult() === "draw") {
      GameLayout.result().classList.contains("draw-result") ||
        GameLayout.result().classList.add("draw-result");
      GameLayout.result().classList.contains("result") &&
        GameLayout.result().classList.remove("draw-result");
    } else {
      GameLayout.result().classList.contains("result") ||
        GameLayout.result().classList.add("result");
      GameLayout.result().classList.contains("draw-result") &&
        GameLayout.result().classList.remove("draw-result");
    }
    GameLayout.result().textContent = this.getResult().toUpperCase();
    const sound = new Sound(this.getResult());
    sound.setAudio();
  }
  /**
   * method to refresh or reset the game, start from the begining
   * @method
   */
  static refresh() {
    GameLayout.playerChoices().forEach((item) => {
      item.classList.remove("selected");
    });
    GameLayout.comChoices().forEach((item) => {
      item.classList.remove("selected");
    });
    if (GameLayout.result().classList.contains("result")) {
      GameLayout.result().classList.remove("result");
    } else if (GameLayout.result().classList.contains("draw-result")) {
      GameLayout.result().classList.remove("draw-result");
    } else {
      GameLayout.result().className = "";
    }
    GameLayout.result().textContent = "VS";
  }
}

/**
 * a class to play sound based on the result
 */
class Sound {
  #sound;
  constructor(sound) {
    this.#sound = sound;
  }
  /**
   * method to play the audio
   * @method
   */
  setAudio() {
    GameLayout.playSound(this.#sound).currentTime = 0;
    GameLayout.playSound(this.#sound).play();
  }
}

/**
 * a class to start the game
 */
class Game {
  constructor() {
    this.playerOneChoices = "";
    this.playerTwoChoices = "";
  }
  /**
   * method to start the game after player 1 choose the choices beetwen batu, gunting and kertas
   * @method
   */
  start() {
    GameLayout.playerOneChoices().forEach((item) => {
      item.addEventListener("click", () => {
        Result.refresh();
        item.classList.add("selected");
        this.playerOneChoices = item.getAttribute("value");
      });
    });
    GameLayout.playerTwoChoices().forEach((item) => {
      item.addEventListener("click", () => {
        Result.refresh();
        item.classList.add("selected");
        this.playerTwoChoices = item.getAttribute("value");
      });
    });
    const result = new Result(this.playerOneChoices, this.playerTwoChoices);
    result.setResultLayout();
    GameLayout.refresh().addEventListener("click", () => {
      Result.refresh();
    });
  }
}
//start the game
const game = new Game();
game.start();
